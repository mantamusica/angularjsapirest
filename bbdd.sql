CREATE DATABASE IF NOT EXISTS angular_api_rest;
USE angular_api_rest;

CREATE TABLE productos(
    id              INT(255) auto_increment not null,
    nombre          VARCHAR(255),
    descripcion     VARCHAR(255),
    precio          VARCHAR(255),
    imagen          VARCHAR(255),
    CONSTRAINT pk_productos PRIMARY KEY(id)
)ENGINE=InnoDb;