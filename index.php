<?php

require_once './vendor/autoload.php';

$db = new mysqli('localhost', 'root', '', 'angular_api_rest');

//configuracion de cabeceras cors en apache
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if ($method == "OPTIONS") {
    die();
}


$app = new \Slim\Slim();

$app->get("/pruebas", function() use ($app, $db){
    echo "Hola mundo desde Slim Php";
});

$app->get("/probando", function() use ($app){
    echo "Hola mundo desde Slim Php 2";
});

//Listar productos

$app->get('/productos', function() use($db, $app){
    $sql = 'SELECT * FROM productos ORDER BY id DESC;';
    $query = $db->query($sql);

    $productos = array();
    while ($producto = $query->fetch_assoc()){
        $productos[] = $producto; 
    }

    $result = array(
        'status' => 'success',
        'code' => 200,
        'data' => $productos
    );

    //var_dump($productos);
    echo json_encode($result);
});

//Un Producto

$app->get('/producto/:id', function ($id) use ($db, $app) {
    $sql = 'SELECT * FROM productos WHERE id = '.$id.';';
    $query = $db->query($sql);

    $result = array(
        'status' => 'error',
        'code' => 404,
        'data' => 'No existe dicho producto.'
    );

    if($query->num_rows == 1){
        $producto = $query->fetch_assoc();

        $result = array(
            'status' => 'success',
            'code' => 200,
            'data' => $producto
        );
    }

    //var_dump($producto);
    echo json_encode($result);
});

//Eliminar un producto

$app->get('/delete-producto/:id', function ($id) use ($db, $app) {
    $sql = 'DELETE FROM productos WHERE id = ' . $id;
    $query = $db->query($sql);

    if($query){
        $result = array(
            'status' => 'success',
            'code' => 200,
            'data' => 'Elemento borrado con éxito.'
        );
    }else{
        $result = array(
            'status' => 'error',
            'code' => 404,
            'data' => 'Elemento no borrado.'
        );
    }

    //var_dump($producto);
    echo json_encode($result);
});

//Actualizar un producto

$app->post('/update-producto/:id', function ($id) use ($app, $db) {

    $json = $app->request->post('json');
    $data = json_decode($json,true);

    //var_dump($data["nombre"]);

    
    $sql = "UPDATE productos SET ".
                "nombre = '{$data["nombre"]}',".
                "descripcion = '{$data["descripcion"]}',";

                if(isset($data{'iamgen'})){
                    $sql .= "imagen = '{$data["imagen"]}',";
                }
                $sql = "precio = '{$data["precio"]}', imagen = '{$data["imagen"]}'".
                "WHERE id = {$id}";

    $query = $db->query($sql);

    //var_dump($sql);

    if ($query) {
        $result = array(
            'status' => 'success',
            'code' => 200,
            'data' => 'Elemento actualizado con éxito.'
        );
    } else {
        $result = array(
            'status' => 'error',
            'code' => 404,
            'data' => 'Elemento no actualizado.'
        );
    }

    echo json_encode($result);
});

//Subir una imagen

$app->post('/upload-file', function () use ($app, $db) {
        $result = array(
            'status' => 'error',
            'code' => 404,
            'data' => 'El archivo no ha podido subirse.'
        );
        if(isset($_FILES['uploads'])){
            $piramideUploader = new PiramideUploader();

            $upload = $piramideUploader->upload('image', 'uploads', 'uploads', array('image/jpeg', 'image/png', 'image/jpg'));
            $file = $piramideUploader->getInfoFile();
            $file_name = $file['complete_name'];
            if(isset($upload) && $upload['uploaded'] == false){
                $result = array(
                    'status' => 'error',
                    'code' => 404,
                    'data' => 'El archivo no ha podido subirse.'
                );
            } else {
                $result = array(
                    'status' => 'success',
                    'code' => 200,
                    'data' => 'El archivo ha subido perfectamente.',
                    'filename' => $file_name
                );
            }
            
        }

        echo json_encode($result);
});

//Guardar productos
$app->post('/productos', function() use ($app, $db){ 

    $json = $app->request->post('json');
    $data = json_decode($json, true);

   if(!isset($data['nombre'])){
        $data['nombre']=null;
    }
    if(!isset($data['descripcion'])){
        $data['descripcion']=null;
    }
    if(!isset($data['precio'])){
        $data['precio']=null;
    }
    if(!isset($data['imagen'])){
        $data['imagen']=null;
    }

    $query = "INSERT INTO productos VALUES (
            null,".
            "'{$data['nombre']}',".
            "'{$data['descripcion']}',".
            "'{$data['precio']}',".
            "'{$data['imagen']}'".
            ");";

    $insert = $db->query($query);

    $result = array(
        'status' => 'error',
        'code' => 404,
        'message' => 'Producto no insertado correctamente.'
    );

    if($insert){
        $result = array(
            'status' => 'success',
            'code' => 200,
            'message' => 'Producto insertado correctamente.'
        );
    }


    echo json_encode($result);

});

$app->run();